package yapily.marvel.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.Collection;

@Data
@AllArgsConstructor
@Builder
public class MarvelCharacter {

    private Long id;

    private String name;

    private String description;

    private String imageUrl;

    private Collection<String> stories;

    private Collection<String> events;

    private Collection<String> series;

}
