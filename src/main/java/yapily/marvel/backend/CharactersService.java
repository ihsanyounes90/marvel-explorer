package yapily.marvel.backend;

import yapily.marvel.model.MarvelCharacter;

import java.util.List;
import java.util.Optional;

public interface CharactersService {

    List<MarvelCharacter> getCharacters(int offset, int limit);

    List<MarvelCharacter> getCharacters(int offset, int limit, String text);

    long countCharacters();

    long countCharacters(String text);

    Optional<MarvelCharacter> getCharacter(Long id);

}
