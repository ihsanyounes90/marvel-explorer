package yapily.marvel.backend;

import lombok.extern.slf4j.Slf4j;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import yapily.marvel.model.MarvelCharacter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@Service
public class CharactersServiceImpl implements CharactersService {
    private final List<MarvelCharacter> repository = new ArrayList<>();

    private String apikey;
    private String hash;
    private RestTemplate restTemplate;

    @Autowired
    CharactersServiceImpl(
            @Value("${marvel.apikey}") String apikey,
            @Value("${marvel.hash}") String hash,
            RestTemplate restTemplate){
        this.apikey = apikey;
        this.hash = hash;
        this.restTemplate = restTemplate;

        log.info("Loading data from Marvel..");
        try {
            populateCharacters();
        } catch (Exception e){
            log.error("Error during loading data from Maven", e);
        }
        log.info("Done.");

    }

    @Override
    public List<MarvelCharacter> getCharacters(int offset, int limit) {
        return getSublist(offset, limit, repository);
    }

    @Override
    public List<MarvelCharacter> getCharacters(int offset, int limit, String text) {
        if (text == null || text.isEmpty()) {
            return getCharacters(offset, limit);
        }
        final List<MarvelCharacter> marvelCharacters = getMarvelCharacterSearchStream(text).collect(Collectors.toList());
        return getSublist(offset, limit, marvelCharacters);
    }

    @Override
    public long countCharacters() {
        return repository.size();
    }

    @Override
    public long countCharacters(String text) {
        if (text == null || text.isEmpty()) {
            return countCharacters();
        }
        return getMarvelCharacterSearchStream(text).count();
    }


    @Override
    public Optional<MarvelCharacter> getCharacter(Long id) {
        return repository.stream()
                         .filter(c -> c.getId().equals(id))
                         .findAny();
    }

    private List<MarvelCharacter> getSublist(int offset, int limit, List<MarvelCharacter> marvelCharacters) {
        if (offset < marvelCharacters.size()) {
            int toIndex = offset + limit;
            if (toIndex > marvelCharacters.size()) {
                toIndex = marvelCharacters.size();
            }
            return marvelCharacters.subList(offset, toIndex);
        } else {
            return new ArrayList<>();
        }
    }

    private Stream<MarvelCharacter> getMarvelCharacterSearchStream(String text) {
        return repository.stream().filter(c -> c.getDescription().toLowerCase().contains(text.toLowerCase()) || c.getName().toLowerCase().contains(text.toLowerCase()));
    }

    private void populateCharacters()
    {
        final String uri = "https://gateway.marvel.com/v1/public/characters?apikey=%s&hash=%s&ts=1&limit=%d&offset=%d";

        final int limit = 100;
        int offset = 0;
        int total = -1;

        do {
            JSONObject result = new JSONObject(restTemplate.getForObject(String.format(uri, apikey, hash, limit, offset), String.class));
            log.info(result.toString());

            JSONObject data = result.getJSONObject("data");
            total = data.getInt("total");

            offset += limit;

            JSONArray results = data.getJSONArray("results");
            for (int i = 0 ; i < results.length(); i++) {
                final JSONObject character = results.getJSONObject(i);

                repository.add(MarvelCharacter.builder()
                                .id(character.getLong("id"))
                                .name(character.getString("name"))
                                .description(character.getString("description"))
                                .imageUrl(character.getJSONObject("thumbnail").getString("path") + "." + character.getJSONObject("thumbnail").getString("extension"))
                                .series(getNameFromArray(character.getJSONObject("series")))
                                .events(getNameFromArray(character.getJSONObject("events")))
                                .stories(getNameFromArray(character.getJSONObject("stories"))).build());
            }

        } while(offset < total);
    }
    private static Collection<String> getNameFromArray(final JSONObject data){
        Collection<String> result = new ArrayList<>();
        final JSONArray items = data.getJSONArray("items");
        for (int i = 0 ; i < items.length(); i++) {
            result.add(items.getJSONObject(i).getString("name"));
        }
        return result;
    }

}
