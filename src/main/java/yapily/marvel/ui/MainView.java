package yapily.marvel.ui;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.HtmlImport;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouterLink;
import org.springframework.beans.factory.annotation.Autowired;
import yapily.marvel.backend.CharactersService;
import yapily.marvel.model.MarvelCharacter;

import java.util.List;

@Route
@HtmlImport("frontend://styles/shared-styles.html")
public class MainView extends VerticalLayout {

    private static final int PAGE_SIZE = 25;
    private Integer position = 0;

    private VerticalLayout charactersLayout;
    private CharactersService charactersService;
    private Button loadMoreButton;
    private TextField searchTextField;
    private String textSearch;

    public MainView(@Autowired CharactersService charactersService) {
        this.charactersService = charactersService;

        charactersLayout = new VerticalLayout();

        searchTextField = new TextField();
        searchTextField.setPlaceholder("Search.. ");
        searchTextField.setValueChangeMode(ValueChangeMode.EAGER);
        searchTextField.addValueChangeListener(event -> {
            textSearch = searchTextField.getValue();
            charactersLayout.removeAll();
            position = 0;
            loadMoreButton.setVisible(true);
            loadMore(textSearch);
        });

        loadMoreButton = new Button("Load more", l -> loadMore(textSearch));

        add(searchTextField, charactersLayout, loadMoreButton);

        loadMore(null);
    }

    private void loadMore(String textSearch) {
        List<MarvelCharacter> characters = charactersService.getCharacters(position, PAGE_SIZE, textSearch);
        characters.stream()
                  .map(this::buildCharacterComponent)
                  .forEachOrdered(charactersLayout::add);

        position += characters.size();
        if (position >= charactersService.countCharacters(textSearch)) {
            loadMoreButton.setVisible(false);
        }

    }

    private Div buildCharacterComponent(MarvelCharacter character) {
        Div div = new Div();
        div.addClassNames("character", "list-item");

        Label name = new Label(character.getName());

        RouterLink button = new RouterLink("View",
                                           CharacterView.class,
                                           character.getId());
        div.add(name, button);
        return div;
    }

}
