package yapily.marvel.ui;

import com.vaadin.flow.component.html.*;
import com.vaadin.flow.component.listbox.ListBox;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;
import yapily.marvel.backend.CharactersService;

@Route("character")
public class CharacterView extends VerticalLayout implements HasUrlParameter<Long> {

    private H1 name;
    private Paragraph description;
    private Image image;
    private ListBox events;
    private ListBox series;
    private ListBox stories;
    private CharactersService charactersService;


    public CharacterView(@Autowired CharactersService charactersService) {
        this.charactersService = charactersService;
        NativeButton button = new NativeButton("Back");
        button.addClickListener(e -> {
            button.getUI().ifPresent(ui -> ui.navigate(MainView.class));
        });

        description = new Paragraph();
        name = new H1();

        image = new Image();
        image.addClassName("character-image");

        stories = new ListBox();
        events = new ListBox();
        series = new ListBox();

        add(button, name, description, image, buildListSection("Stories:", stories), buildListSection("Events:", events), buildListSection("Series:", series));
    }


    @Override
    public void setParameter(BeforeEvent event, Long parameter) {
        charactersService.getCharacter(parameter)
                         .ifPresent(c -> {
                             name.setText(c.getName());
                             description.setText(c.getDescription());
                             image.setSrc(c.getImageUrl());
                             stories.setItems(c.getStories());
                             events.setItems(c.getEvents());
                             series.setItems(c.getSeries());
                            });
    }


    private Div buildListSection(String title, ListBox listBox) {
        Div div = new Div();
        div.addClassNames("character");

        Label name = new Label(title);
        listBox.setEnabled(false);

        div.add(name, listBox);
        return div;
    }

}
