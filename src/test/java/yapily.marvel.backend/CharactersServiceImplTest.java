package yapily.marvel.backend;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.web.client.RestTemplate;
import yapily.marvel.model.MarvelCharacter;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CharactersServiceImplTest {

    @Mock
    RestTemplate restTemplate;

    CharactersService CharactersServiceImpl;

    @Before
    public void setup(){
        when(restTemplate.getForObject(anyString(), any())).thenReturn("{\"data\": {\"total\": 3, \"results\": [" +
                "{\"id\": 1, \"name\": \"name 1\", \"description\": \"description 1\", \"thumbnail\": {\"path\": \"http://test\", \"extension\": \"jpg\"}, \"series\": {\"items\": []}, \"stories\": {\"items\": []}, \"events\": {\"items\": []} }," +
                "{\"id\": 2, \"name\": \"name 2\", \"description\": \"description 2\", \"thumbnail\": {\"path\": \"http://test\", \"extension\": \"jpg\"}, \"series\": {\"items\": []}, \"stories\": {\"items\": []}, \"events\": {\"items\": []} }," +
                "{\"id\": 3, \"name\": \"name 3\", \"description\": \"description 3\", \"thumbnail\": {\"path\": \"http://test\", \"extension\": \"jpg\"}, \"series\": {\"items\": []}, \"stories\": {\"items\": []}, \"events\": {\"items\": []} }" +
                "] }}}");
        CharactersServiceImpl = new CharactersServiceImpl("", "", restTemplate);
    }

    @Test
    public void testGetAllCharacters() {
        final List<MarvelCharacter> characters = CharactersServiceImpl.getCharacters(0, 3);
        assertEquals(3, characters.size());
        assertEquals(1, characters.get(0).getId().longValue());
        assertEquals(2, characters.get(1).getId().longValue());
        assertEquals("name 1", characters.get(0).getName());
        assertEquals("description 1", characters.get(0).getDescription());
    }

    @Test
    public void testGetCountAllCharacters() {
        assertEquals(3, CharactersServiceImpl.countCharacters());
    }

    @Test
    public void testSearchByNameCharacters() {
        final List<MarvelCharacter> characters = CharactersServiceImpl.getCharacters(0, 3, "name 1");
        assertEquals(1, characters.size());
        assertEquals(1, characters.get(0).getId().longValue());
    }

    @Test
    public void testSearchByDescriptionCharacters() {
        final List<MarvelCharacter> characters = CharactersServiceImpl.getCharacters(0, 3, "description 1");
        assertEquals(1, characters.size());
        assertEquals(1, characters.get(0).getId().longValue());
    }

    @Test
    public void testGetCountAllCharactersWithSearch() {
        assertEquals(1, CharactersServiceImpl.countCharacters("name 1"));
    }

    @Test
    public void testGetCharacter() {
        final MarvelCharacter character = CharactersServiceImpl.getCharacter(1l).get();
        assertEquals(1, character.getId().longValue());
        assertEquals("name 1", character.getName());
        assertEquals("description 1", character.getDescription());
        assertEquals("http://test.jpg", character.getImageUrl());
    }

}