# Project Base for the Yapily Marvel code test

Import the project to the IDE of your choosing as a Maven project. 

In `application.properties` place apikey and hash from maven, for more information: https://developer.marvel.com/documentation/authorization


Run application using `mvn spring-boot:run` or directly running Application class from your IDE.

Open http://localhost:8080/ in browser


For documentation on using Vaadin Flow and Spring, visit [vaadin.com/docs](https://vaadin.com/docs/v10/flow/spring/tutorial-spring-basic.html)

For more information on Vaadin Flow, visit https://vaadin.com/flow.
